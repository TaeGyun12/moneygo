import unittest
from Collector import *


class CollectorTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.collector: Collector = Collector()
        cls.test_stock_code = "005930",
        cls.test_stock_listed_date = "19750611"
        cls.app = QApplication(sys.argv)
        return super().setUpClass()

    def test_get_investor_trend_period_with_beforeData(self):
        """
        처음 업데이트를 하는 경우.
        :return:
        """
        # Preconditon
        base_info = {'code': '352820', 'company_name': '빅히트', 'listing_date': '20201015', 'update_date': 0,
                     'business': '오디오물 출판 및 원판 녹음업'}
        MysqlConfig.DB_STOCK_TREND_INFO = 'test'

        # Do
        # df_trend_model = self.collector.make_whole_investor_trend_period_per_stock(base_info, datetime.today())
