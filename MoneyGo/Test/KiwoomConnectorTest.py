import unittest
from MoneyGo.Connector.KiwoomConnector import *


class KiwoomConnectorTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.test_stock_code = "352820"
        cls.app = QApplication(sys.argv)
        cls.kiwoom_connector: KiwoomConnector = KiwoomConnector()
        return super().setUpClass()

    def test_get_kospi_code(self):
        """
        Kospi 정보중 종목 Code, 이름, 상장일을 잘 가져오는지 debugging
        :return: kospi 종목 정보가 담긴 dataFrame
        """
        df_kospi: pd.DataFrame = self.kiwoom_connector.get_all_kospi_informations()
        print(df_kospi.head())
        self.assertIsNotNone(df_kospi)

    def test_get_kosdaq_code(self):
        """
        Kospi 정보중 종목 Code, 이름, 상장일을 잘 가져오는지 debugging
        :return: kospi 종목 정보가 담긴 dataFrame
        """
        df_kosdaq: pd.DataFrame = self.kiwoom_connector.get_all_kosdaq_informations()
        print(df_kosdaq.head())
        self.assertIsNone(df_kosdaq)

    def test_get_investor_trend_per_day(self):
        """
        상장된 종목의 투자자별 매매동향을 가져온다.
        :return:
        """
        self.kiwoom_connector.get_investor_trend_per_day("20201017", self.test_stock_code)
        result: dict = self.kiwoom_connector.get_inverstor_trend_data()
        self.assertIsNotNone(result)

    def test_get_investor_trend_per_day_not_listing_stock(self):
        """
        상장되지 않은 종목의 투자자별 매매동향을 가져온다.
        :return:
        """
        self.kiwoom_connector.get_investor_trend_per_day("20200101", self.test_stock_code)
        result: dict = self.kiwoom_connector.get_inverstor_trend_data()
        self.assertEqual(len(result.items()), 0)


if __name__ == '__main__':
    unittest.main()
