import unittest
from MoneyGo.Util.Logger import *
from Scheduler import *
from MoneyGo.Config.config import *


class SchedulerTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.scheduler: Scheduler = Scheduler()
        cls.test_file_path = os.path.abspath('testDummy/SchedulerMock.py')
        cls.collector_mock_path = os.path.abspath('testDummy/KiwoomMock.py')
        cls.collector_path = os.path.abspath("../../Collector.py")

    def test_end_code(self):
        ret = self.scheduler.run_money_go(self.test_file_path, ExitCode.END)
        self.assertEqual(ret.stderr.strip(), ExitCode.END)

    def test_retry_code(self):
        ret = self.scheduler.run_money_go(self.test_file_path, ExitCode.RETRY)
        self.assertEqual(ret.stderr.strip(), ExitCode.RETRY)

    def test_kiwoom_retry_login(self):
        is_over, run_count = False, 0

        while not is_over:
            ret = self.scheduler.run_money_go(self.collector_mock_path , '3335')

            if ret.stderr.strip() == ExitCode.END:
                is_over = True
            elif run_count == 5:
                break
            run_count += 1
            timer.sleep(2)

        self.assertEqual(run_count, 5)

    def test_collector_make_stock_info(self):
        is_not_over, stock_code = True, 'kospi'

        while is_not_over:
            ret = self.scheduler.run_money_go(self.collector_path, stock_code)

            if ret.stderr.strip().endswith(ExitCode.END):
                is_not_over = False

            timer.sleep(0.5)
