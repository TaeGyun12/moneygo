from MoneyGo.Collector import *


class KiwoomTestMock:
    def __init__(self):
        self.app = QApplication(sys.argv)
        self._kiwoom_connector: KiwoomConnector = KiwoomConnector()
        self.stock_code = "352820"
        self.start_day = "20201015"

    def test(self):
        for v in range(10000):
            ret = self._kiwoom_connector.get_investor_trend_per_day(self.start_day, self.stock_code)

            if ret is False:
                exit(ExitCode.RETRY)


if __name__ == "__main__":
    kiwoomMock: KiwoomTestMock = KiwoomTestMock()
    kiwoomMock.test()