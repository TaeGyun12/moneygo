import sys
from MoneyGo.Config.config import *


class Test:
    def __init__(self):
        self.result = True

    def hang_test(self):
        while True:
            pass

    def end_reuslt(self):
        self.result = True
        exit(ExitCode.END)

    def retry_result(self):
        self.result = False
        exit(ExitCode.RETRY)


if __name__ == "__main__":
    mode = sys.argv[1]
    test: Test = Test()

    # run test
    if mode == ExitCode.HANG:
        test.hang_test()
    elif mode == ExitCode.END:
        test.end_reuslt()
    elif mode == ExitCode.RETRY:
        test.retry_result()