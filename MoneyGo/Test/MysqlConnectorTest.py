import sys
import unittest
from datetime import *
from ..Connector.MySqlConnector import MysqlConnector
from ..Connector.KiwoomConnector import KiwoomConnector, QApplication
from ..Config.config import MysqlConfig
from ..Model.Stockitem import *
from ..Model.BaseStockInfo import *


class MysqlConnectorTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.sql_connector: MysqlConnector = MysqlConnector()
        cls.app = QApplication(sys.argv)
        cls.kiwoom_connector: KiwoomConnector = KiwoomConnector()

        MysqlConfig.DB_STOCK_TREND_INFO = 'test'
        MysqlConfig.DB_STOCK_BASE_INFO = 'test'
        cls.test_db = 'test'
        cls.test_stock_code = '352820'
        cls.stock_item_test_data: pd.DataFrame = StockItem.get_stock_item_data_frame()
        cls.stock_item_test_data.loc[0] = ['1' for _ in StockItem.COLUMNS]
        cls.base_stock_info_test_data = pd.DataFrame(BaseStockInfo.get_empty_base_stock_info(),
                                                     columns=BaseStockInfo.KEYS, index=[0])

        # Test용 DB가 없으면 만든다
        if cls.test_db not in cls.sql_connector.show_database():
            cls.sql_connector.create_database(cls.test_db)

    def test_insert_kospi_stockInfo(self):
        # Preconditon
        self.sql_connector.drop_table(self.test_db, MysqlConfig.TABLE_KOSPI_BASE_INFO)

        # Do
        kospi_info: pd.DataFrame = self.kiwoom_connector.get_all_kospi_informations()
        self.sql_connector.register_whole_stock_base_info(MysqlConfig.TABLE_KOSPI_BASE_INFO, kospi_info)

        # Assert
        registerd_value = self.sql_connector.get_whole_stock_base_info(MysqlConfig.TABLE_KOSPI_BASE_INFO)
        result = registerd_value == kospi_info
        self.assertTrue(False not in list(result.values.flat))

    def test_get_stock_info(self):
        # Precondition
        expected_code = '0'
        self.sql_connector.drop_table(self.test_db, MysqlConfig.TABLE_KOSPI_BASE_INFO)
        self.sql_connector.register_whole_stock_base_info(MysqlConfig.TABLE_KOSPI_BASE_INFO,
                                                          self.base_stock_info_test_data)

        # Do
        result = self.sql_connector.get_stock_base_info(MysqlConfig.TABLE_KOSPI_BASE_INFO, expected_code)

        # Assert
        self.assertTrue(result.loc[0, 'code'] == expected_code)

    def test_update_date(self):
        # Precondition
        expected_update_date = 19931218
        current_db_list = self.sql_connector.show_database()
        if self.test_db not in current_db_list:
            self.sql_connector.create_database('test')

        # Do
        self.sql_connector.register_stock_update_date(MysqlConfig.TABLE_KOSPI_BASE_INFO, self.test_stock_code,
                                                      expected_update_date)
        result = self.sql_connector.get_stock_base_info(MysqlConfig.TABLE_KOSPI_BASE_INFO, self.test_stock_code)

        # Assert
        self.assertTrue(result['update_date'] == expected_update_date)

        # delete
        self.sql_connector.register_stock_update_date(MysqlConfig.TABLE_KOSPI_BASE_INFO, self.test_stock_code, 0)

    def test_get_stock_latest_trend(self):
        # Precondition
        expected_lastest_date = '1'
        self.sql_connector.drop_table(self.test_db, self.test_stock_code)

        self.sql_connector.register_stock_trend(self.test_stock_code, self.stock_item_test_data)

        # Do
        result = self.sql_connector.get_stock_latest_trend(self.test_stock_code, expected_lastest_date)

        # Assert
        self.assertTrue(result.iloc[0, 0] == expected_lastest_date)

    def test_create_stock_market_day(self):
        # Precondition
        self.sql_connector.drop_table(self.test_db, MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY)

        # Do
        self.sql_connector.create_last_stock_market_day()

        result = self.sql_connector.check_table(self.test_db, MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY)

        self.assertTrue(result == True)

    def test_update_stock_market_day(self):
        # Precondition
        self.sql_connector.drop_table(self.test_db, MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY)
        self.sql_connector.create_last_stock_market_day()
        expected_date = '19931218'

        # Do
        self.sql_connector.register_stock_market_date(expected_date)
        result = self.sql_connector.get_stock_market_date()

        self.assertTrue(expected_date == result)

    def test_not_update_kospi_lists_in_all_updated(self):
        # Preconditon
        self.sql_connector.drop_table(self.test_db, MysqlConfig.TABLE_KOSPI_BASE_INFO)

        kospi_info: pd.DataFrame = self.kiwoom_connector.get_all_kospi_informations()
        self.sql_connector.register_whole_stock_base_info(MysqlConfig.TABLE_KOSPI_BASE_INFO, kospi_info)
        expected_date = "0"

        # Do
        result = self.sql_connector.get_not_updated_stock_list(MysqlConfig.TABLE_KOSPI_BASE_INFO, expected_date)

        # Assert
        self.assertTrue(len(result) == 0)

    def test_not_update_kospi_lists_in_all_not_updated(self):
        # Preconditon
        self.sql_connector.drop_table(self.test_db, MysqlConfig.TABLE_KOSPI_BASE_INFO)

        kospi_info: pd.DataFrame = self.kiwoom_connector.get_all_kospi_informations()
        self.sql_connector.register_whole_stock_base_info(MysqlConfig.TABLE_KOSPI_BASE_INFO, kospi_info)
        expected_list_count = len(kospi_info)

        # Do
        result = self.sql_connector.get_not_updated_stock_list(MysqlConfig.TABLE_KOSPI_BASE_INFO, f"{datetime.today():%Y%m%d}")

        # Assert
        self.assertTrue(len(result) == expected_list_count)