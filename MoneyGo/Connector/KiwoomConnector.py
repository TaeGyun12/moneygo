import sys
import copy
import time as timer
from ..Model.Stockitem import *
from PyQt5.QAxContainer import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from ..Util.Utility import *
from ..Util.Logger import *
from ..Config.config import *


def inquery_scheduler(query_function):
    def inner(self, *arg):
        """
        키움 서버에 Query시, Timeout을 최대한 안발생시키거고 발생할 경우 처리하기 위한 기능
        :param self: KiwoomConnector
        :param arg:
        :return:
        """

        #  Timer 설정 / 쿼리 조절
        self.query_count += 1

        if self.timer:
            self.timer.stop()
            self.timer.deleteLater()

        if self.query_count == ApiQuery.TR_MAX_QUERY:
            return -200
        else:
            timer.sleep(ApiQuery.TR_TIME_INTERVAL)

        self.timer: QTimer = QTimer()
        self.timer.timeout.connect(self.quit_tr_event_loop)
        self.timer.setSingleShot(True)
        self.timer.start(3000)

        #  쿼리 수행
        ret = query_function(self, *arg)

        #  타임 아웃 핸들링을 위한 타이머 해제
        self.timer.stop()

        return ret

    return inner


class KiwoomConnector(QAxWidget):
    def __init__(self):
        super().__init__()
        self.logger = Logger.get_logger('KiwoomConnector')
        # API 연결을 위한 변수
        self.setControl("KHOPENAPI.KHOpenAPICtrl.1")
        self._stock_investor_trend_model = {}
        self._kospi_list = self._kosdaq_list = None

        # Query 조절 용 변수
        self.timer: QTimer = None
        self.query_count = 0

        self._initialize()

    def _initialize(self):
        """
        초기 환경 설정을 해주는 함수
        :return:
        """
        # Event 함수 설정 #
        self.OnEventConnect.connect(self._conn_event)
        self.OnReceiveTrData.connect(self._on_receive_tr_data)

        # Login #
        self._login()

    def exit_event_processing(self):
        self.conn_event_loop.exit()

    def get_inverstor_trend_data(self):
        return self._stock_investor_trend_model

    def _login(self):
        self.dynamicCall("CommConnect()")
        self.conn_event_loop = QEventLoop()
        self.conn_event_loop.exec_()

    def _conn_event(self, nErrCode):
        """
        login call back 메소드
        :param nErrCode: 
        :return: 
        """
        if nErrCode == 0:
            self.logger.info('Login Success')
        else:
            self.logger.info('Login Fail')
            exit(ExitCode.RETRY)
        self.conn_event_loop.exit()

    def _set_input_value(self, input_name: str, input_value: str):
        """
        Request 전에, Request의 Parameter를 설정해주는 함수
        :param input_name:
        :param input_value:
        :return:
        """
        self.dynamicCall("SetInputValue(QString, QString)", input_name, input_value)

    def quit_tr_event_loop(self):
        self.tr_event_loop.quit()

    @inquery_scheduler
    def _comm_rq_data(self, rq_name: str, tr_code: str, next_value: int, screen_number: str) -> int:
        """
        Kiwoom 증권에 정보를 요청하는데 사용
        :param rq_name:
        :param tr_code:
        :param next_value:
        :param screen_number:
        :return:
        """
        status = self.dynamicCall("CommRqData(QString, QString, int, QString", rq_name, tr_code, next_value, screen_number)
        self.tr_event_loop = QEventLoop()
        self.tr_event_loop.exec_()
        return status

    def _get_comm_data(self, tr_code: str, record_name: str, index: int, item_name: str) -> object:
        return self.dynamicCall("GetCommData(QString, QString, int, QString)", tr_code, record_name, index, item_name)

    def _on_receive_tr_data(self, screen_number: str, rq_name: str, tr_code: str, record_name: str, prev_next: str, data_len: int, err_code: str, msg1: str, msg2: str):
        if rq_name == "opt10059_req":
            self._opt_10059_req(tr_code, rq_name)

        self.tr_event_loop.exit()

    def _opt_10059_req(self, tr_code: str, rq_name: str):
        '''
        해당 날짜에 대한 거래 투자자별 수급 정보를 가져온다.
        해당 날에 데이터가 없으면 date에 '' 값이 오므로 따로 처리한다.
        :param tr_code:
        :param rq_name:
        :return:
        '''
        date, query_result = self._get_comm_data(tr_code, rq_name, 0, "일자").strip(), {}

        if date:
            query_result = {"일자": date,
                            "현재가": Utility.change_value_format(self._get_comm_data(tr_code, rq_name, 0, "현재가").strip()),
                            "등락율": self._get_comm_data(tr_code, rq_name, 0, "등락율").strip(),
                            "누적거래량": self._get_comm_data(tr_code, rq_name, 0, "누적거래량").strip(),
                            "누적거래대금": self._get_comm_data(tr_code, rq_name, 0, "누적거래대금").strip(),
                            "개인투자자": self._get_comm_data(tr_code, rq_name, 0, "개인투자자").strip(),
                            "외국인투자자": self._get_comm_data(tr_code, rq_name, 0, "외국인투자자").strip(),
                            "금융투자": self._get_comm_data(tr_code, rq_name, 0, "금융투자").strip(),
                            "보험": self._get_comm_data(tr_code, rq_name, 0, "보험").strip(),
                            "투신": self._get_comm_data(tr_code, rq_name, 0, "투신").strip(),
                            "은행": self._get_comm_data(tr_code, rq_name, 0, "은행").strip(),
                            "연기금등": self._get_comm_data(tr_code, rq_name, 0, "연기금등").strip()}

            self._stock_investor_trend_model = query_result
        else:
            self._stock_investor_trend_model = {}

    def get_all_kospi_informations(self) -> pd.DataFrame:
        """
        kospi 종목을 '회사명', '종목 코드', '상장일'의 데이터 프레임으로 리턴
        :return: kospi 종목 정보가 담긴 dataFrame
        """
        self._kospi_list = pd.read_html(
            'http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=13&marketType=stockMkt',
            header=0)[0]
        self._kospi_list = self._kospi_list.rename(columns={'회사명': 'company_name', '종목코드': 'code',
                                                            '상장일': 'listing_date', '업종': 'business'})

        self._kospi_list.code = self._kospi_list.code.map(lambda x: f'{x:06d}')
        self._kospi_list.listing_date = self._kospi_list.listing_date.map(lambda x: x.replace('-', ''))
        self._kospi_list['update_date'] = '0'
        df_kospi = self._kospi_list[['code', 'company_name', 'listing_date', 'update_date', 'business']]

        return copy.deepcopy(df_kospi)

    def get_all_kosdaq_informations(self) -> pd.DataFrame:
        """
        kosdaq 종목을 '회사명', '종목 코드', '상장일'의 데이터 프레임으로 리턴
        :return: kosdaq 종목 정보가 담긴 dataFrame
        """
        self._kosdaq_list = pd.read_html(
            'http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=13&marketType=kosdaqMkt',
            header=0)[0]
        self._kosdaq_list = self._kosdaq_list.rename(columns={'회사명': 'company_name', '종목코드': 'code',
                                                              '상장일': 'listing_date', '업종': 'business'})

        self._kosdaq_list.code = self._kosdaq_list.code.map(lambda x: f'{x:06d}')
        self._kosdaq_list.listing_date = self._kosdaq_list.listing_date.map(lambda x: x.replace('-', ''))
        self._kosdaq_list['update_date'] = '0'
        df_kosdaq = self._kosdaq_list[['code', 'company_name', 'listing_date', 'update_date', 'business']]

        return copy.deepcopy(df_kosdaq)

    def get_investor_trend_per_day(self, date: str, stock_code: str):
        """
        종목별로 투자자별 매매동향을 가져온다.
        :return: 해당 종목에 대한 투자자별 순매수
        """
        self._stock_investor_trend_model = {}
        self._set_input_value("일자", date)
        self._set_input_value("종목코드", stock_code)
        self._set_input_value("금액수량구분", "2")  # 1: 금액, 2: 수량
        self._set_input_value("매매구분", "0")  # 0: 순매수, 1: 매수. 2: 매도
        self._set_input_value("단위구분", "1")  # 1: 단주, 1000: 천주

        status = self._comm_rq_data("opt10059_req", "opt10059", 0, "0101")

        return True if status == 0 else False


if __name__ == "__main__":
    app = QApplication(sys.argv)
