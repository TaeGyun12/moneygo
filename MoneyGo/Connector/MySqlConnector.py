from sqlalchemy import *
from ..Config.config import MysqlConfig
from ..Util.Logger import *
import pandas as pd
import pymysql


class MysqlConnector:
    def __init__(self):
        self._connector = pymysql.connect(host=MysqlConfig.HOST, port=MysqlConfig.PORT, user=MysqlConfig.USER,
                                          passwd=MysqlConfig.PASSWORD, charset=MysqlConfig.CHARSET)
        self.logger = Logger.get_logger('MysqlConnector')
        self.cursor = self._connector.cursor(pymysql.cursors.DictCursor)
        self.engine = None

    def _initialize_db_engine(self, db_name: str) -> None:
        '''
        SQLScheme 사용하기 위해 특정 DB에 연결.
        :param db_name:
        :return:
        '''
        self.engine = create_engine("mysql+pymysql://" + MysqlConfig.USER + ":" + MysqlConfig.PASSWORD + "@" +
                                    MysqlConfig.HOST + ":" + str(MysqlConfig.PORT) + '/' +
                                    f"{db_name}", encoding='utf-8')

    def create_database(self, db_name: str) -> bool:
        current_db_list = self.show_database()
        if db_name in current_db_list:
            return False

        sql_query = f"CREATE DATABASE {db_name}"
        self.cursor.execute(sql_query)
        self._connector.commit()

        return True

    def drop_database(self, db_name: str):
        sql_query = f"DROP DATABASE {db_name}"
        self.cursor.execute(sql_query)
        self._connector.commit()

        return True

    def show_database(self) -> list:
        sql_query = "SHOW DATABASES"
        self.cursor.execute(sql_query)
        self._connector.commit()

        result = self.cursor.fetchall()
        return [name for value in result for name in value.values()]

    def check_table(self, db_name: str, table_name: str) -> bool:
        '''
        특정 db안에 해당 table이 있는지 검사합니다.
        :param db_name:
        :param table_name:
        :return:
        '''
        sql_query = f"SHOW TABLES IN {db_name} LIKE \"{table_name}\""
        try:
            self.cursor.execute(sql_query)
            self._connector.commit()
            result = self.cursor.fetchall()

            return table_name in result[0].values() if result else False
        except Exception as e:
            self.logger.error(f"{table_name} is not in {db_name}, {e}")
            return False

    def drop_table(self, db_name: str, table_name: str) -> bool:
        '''
        특정 db안에 있는 table을 삭제합니다.
        :param db_name:
        :param table_name:
        :return:
        '''
        sql_query = f"DROP TABLE {db_name}.{table_name}"
        try:
            result = self.cursor.execute(sql_query)
            self._connector.commit()
            return True
        except Exception as e:
            self.logger.error(f"Drop {db_name}.{table_name} Error Occured, {e}")
            return False

    def create_last_stock_market_day(self):
        """
        stock_makret table을 생성합니다.
        :param db_name:
        :param table_name:
        :return:
        """
        if not self.check_table(MysqlConfig.DB_STOCK_BASE_INFO, MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY):
            try:
                self._initialize_db_engine(MysqlConfig.DB_STOCK_BASE_INFO)
                data = {'last_date': ['0']}
                table_data: pd.DataFrame = pd.DataFrame(data, columns=['last_date'])
                table_data.to_sql(name=MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY, con=self.engine, index=false)

                return True
            except Exception as e:
                self.logger.error(f'{e}')
                return False
        else:
            self.logger.info(f'Table exist')
            return True

    def register_stock_market_date(self, date: str):
        """
        stock_market의 date를 설정합니다.
        :param date: 
        :return: 
        """
        if self.check_table(MysqlConfig.DB_STOCK_BASE_INFO, MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY):
            try:
                sql_query = f"update {MysqlConfig.DB_STOCK_BASE_INFO}.{MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY} " \
                            f"set last_date = {date}"
                self.cursor.execute(sql_query)
                self._connector.commit()
                return True
            except Exception as e:
                self.logger.error(f'{e}')
                return False
        else:
            raise Exception(f'{MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY} not exist')

    def get_stock_market_date(self):
        """
        stock_market의 date를 가져옵니다.
        :return: 
        """
        if self.check_table(MysqlConfig.DB_STOCK_BASE_INFO, MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY):
            try:
                sql_query = f"select * from {MysqlConfig.DB_STOCK_BASE_INFO}.{MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY}"
                self.cursor.execute(sql_query)
                self._connector.commit()
                row = self.cursor.fetchall()

                return row[0]['last_date'] if row else None
            except Exception as e:
                self.logger.error(f'{e}')
                return False
        else:
            self.logger.info(f'{MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY} not exist')
            return False

    def register_whole_stock_base_info(self, table_name: str, base_stock_info: pd.DataFrame) -> bool:
        """
        kosdaq 또는 kospi 의 전 종목 정보를 저장합니다.
        :param table_name:
        :param base_stock_info:
        :return:
        """
        if base_stock_info is not None:
            try:
                self._initialize_db_engine(MysqlConfig.DB_STOCK_BASE_INFO)
                base_stock_info.to_sql(name=table_name, con=self.engine, if_exists='replace', index=false)

                return True
            except Exception as e:
                self.logger.error(f'{table_name}, {e}')
                return False
        else:
            self.logger.error('Stock info is null')
            return False

    def get_whole_stock_base_info(self, table_name: str) -> pd.DataFrame:
        '''
        모든 주식의 기본 정보를 가져옵니다 (종목코드, 회사 이름, 상장일, 업데이트날, 업종)
        :param table_name: kospi or kosdaq
        :return:
        '''
        if self.check_table(MysqlConfig.DB_STOCK_BASE_INFO, table_name):
            try:
                self._initialize_db_engine(MysqlConfig.DB_STOCK_BASE_INFO)
                base_stock_info: pd.DataFrame = pd.read_sql(table_name, con=self.engine)

                return base_stock_info
            except Exception as e:
                self.logger.error(f'{table_name}, {e}')
                return None
        else:
            self.logger.error(f'There is no Table, {table_name}')
            return None

    def get_stock_base_info(self, table_name, stock_code) -> pd.DataFrame:
        '''
        특정 stock_code를 갖는 주식의 기본 정보를 가져옵니다.
        :param table_name:
        :param stock_code:
        :return:
        '''
        if self.check_table(MysqlConfig.DB_STOCK_BASE_INFO, table_name):
            try:
                sql_query = f"select * from {MysqlConfig.DB_STOCK_BASE_INFO}.{table_name} where code = {stock_code}"
                df = pd.read_sql(sql_query, self._connector)

                if len(df) != 1:
                    raise IndexError('Row Count not 1')
                return df
            except Exception as e:
                self.logger.error(f'{table_name}, {stock_code}, {e}')
                raise IndexError('Row Count not 1')
        else:
            self.logger.error(f'There is no Table, {table_name}')

    def register_stock_trend(self, stock_code, trend_model: pd.DataFrame):
        """
        Update된 투자자별 지표 정보를 DataFrame을 활용해서 저장한다.
        :param stock_code:
        :param trend_model:
        :return:
        """
        try:
            self._initialize_db_engine(MysqlConfig.DB_STOCK_TREND_INFO)
            trend_model.to_sql(name=stock_code, con=self.engine, if_exists='append', index=false)
            return True
        except Exception as e:
            raise Exception(f'Fail to register {stock_code} in DB, {e}')

    def get_stock_latest_trend(self, stock_code: str, last_update_date: str) -> pd.DataFrame:
        """
        특정 종목의 마지막 업데이트된 투자자별 지표들을 가져옵니다.
        :param stock_code:
        :param last_update_date:
        :return:
        """
        if self.check_table(MysqlConfig.DB_STOCK_TREND_INFO, stock_code):
            try:
                sql_query = f"select * from {MysqlConfig.DB_STOCK_TREND_INFO}.{stock_code} where 일자 = {last_update_date}"
                df = pd.read_sql(sql_query, self._connector)

                if len(df) != 1:
                    raise IndexError('Row Count not 1')
                return df
            except Exception as e:
                self.logger.info(f'Not Found, {stock_code}, {e}')
        else:
            self.logger.error(f'There is no Table, {stock_code}')

    def register_stock_update_date(self, table_name: str, stock_code: str, update_date: int):
        """
        특정 종목의 마지막 업데이트 날짜를 갱신합니다.
        :param table_name:
        :param stock_code:
        :param update_date:
        :return:
        """
        if self.check_table(MysqlConfig.DB_STOCK_BASE_INFO, table_name):
            try:
                sql_query = f"update {MysqlConfig.DB_STOCK_BASE_INFO}.{table_name} " \
                            f"set update_date ={update_date} where code = {stock_code}"
                self.cursor.execute(sql_query)
                self._connector.commit()
            except Exception as e:
                raise Exception(f'Fail to register {table_name}, {stock_code}, {e}')

    def get_not_updated_stock_list(self, table_name: str, date: str) -> pd.DataFrame:
        """
        kospi, kosdaq 중에서 date보다 작은 update_date 갖는 종목들을 가져옵니다.
        :param table_name:
        :param date:
        :return:
        """
        if self.check_table(MysqlConfig.DB_STOCK_BASE_INFO, table_name):
            try:
                sql_query = f"select * from {MysqlConfig.DB_STOCK_BASE_INFO}.{table_name} where update_date < {date}"
                df = pd.read_sql(sql_query, self._connector)

                return df
            except Exception as e:
                self.logger.info(f'Not Found, {table_name}, {e}')
        else:
            self.logger.error(f'There is no Table, {table_name}')
            raise Exception("There is no Table")
