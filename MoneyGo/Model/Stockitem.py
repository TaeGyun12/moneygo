from ..Model.BaseStockInfo import *
import copy


class StockItem:
    MODEL = {'일자': []}
    TARGET_INVESTORS = ["개인투자자", "외국인투자자", "금융투자", "보험", "투신", "은행", "연기금등"]
    PERSON = "개인투자자"
    COLUMNS = ["일자", "현재가", "등락율", "누적거래량", "누적거래대금", "추_유통물량", "세력합",
               "개인투자자", "개인투자자_누", "개인투자자_최저", "개인투자자_매집량", "개인투자자_최고", "개인투자자_분산율", "개인투자자_주가선도", "개인투자자_평단",
               "외국인투자자", "외국인투자자_누", "외국인투자자_최저", "외국인투자자_매집량", "외국인투자자_최고", "외국인투자자_분산율", "외국인투자자_주가선도", "외국인투자자_평단",
               "금융투자", "금융투자_누", "금융투자_최저", "금융투자_매집량", "금융투자_최고", "금융투자_분산율", "금융투자_주가선도", "금융투자_평단",
               "보험", "보험_누", "보험_최저", "보험_매집량", "보험_최고", "보험_분산율", "보험_주가선도", "보험_평단",
               "투신", "투신_누", "투신_최저", "투신_매집량", "투신_최고", "투신_분산율", "투신_주가선도", "투신_평단",
               "은행", "은행_누", "은행_최저", "은행_매집량", "은행_최고", "은행_분산율", "은행_주가선도", "은행_평단",
               "연기금등", "연기금등_누", "연기금등_최저", "연기금등_매집량", "연기금등_최고", "연기금등_분산율", "연기금등_주가선도",  "연기금등_평단"]

    def __init__(self, code, company_name):
        self.code = code
        self.company_name = company_name
        self._updated_stock_item = None
        self._accumulated_stock_item: dict = None

    @classmethod
    def get_stock_item_data_frame(cls) -> pd.DataFrame:
        return pd.DataFrame(cls.MODEL, columns=cls.COLUMNS)

    @classmethod
    def get_stock_item_dict(cls) -> dict:
        return {v: '0' for v in cls.COLUMNS}

    def initialize_update_info(self, latest_stock_item: pd.DataFrame):
        if latest_stock_item is None:
            self._accumulated_stock_item: dict = self.get_stock_item_dict()
            self._updated_stock_item = self.get_stock_item_data_frame()
        else:
            self._accumulated_stock_item: dict = latest_stock_item.iloc[0].to_dict()
            self._updated_stock_item = copy.deepcopy(latest_stock_item)

    def get_updated_stock_item(self) -> pd.DataFrame:
        """
        현재 까지 업데이트된 주식 지표정보의 DataFrame을 리턴한다.
        :return:
        """
        return copy.deepcopy(self._updated_stock_item)

    def add_current_accumulated_stock_item(self, date: str):
        self._updated_stock_item.loc[date] = list(self._accumulated_stock_item.values())

    def update_indicator_per_investor(self, today_trend_data: dict):
        """
        각 투자자별로 기본 지표 정보 값을 갱신한다.
        기본 지표 : 누적, 최저, 최고, 매집량, 매집률이다.
        :param today_trend_data:
        :return:
        """
        for column in today_trend_data.keys():
            if column in StockItem.TARGET_INVESTORS:
                # 투자자별 순 매수 누적량
                self.set_accumulated_sum(column, today_trend_data[column])

                #  투자자별 최저 순 매수량
                self.set_min_accumulated_purchase(column)

                #  투자자별 현재 매집량
                self.set_current_hodling_amount(column)

                #  투자자별 최대 매집량
                self.set_max_holding_amount(column)

                #  투자자별 매집 / 분산 비율
                self.set_holding_ratio(column)

                #  투자자별 평균 단가
                self.set_average_unit_price(column)

            self._accumulated_stock_item[column] = today_trend_data[column]

    def set_accumulated_sum(self, investor: str, today_investor_trend: str):
        """
        오늘의 투자자 별 순매수 량을 마지막으로 업데이트된 투투자별 누적 순매수량에 더한다.
        :param investor:
        :param today_investor_trend:
        :param accumulated_stock_item:
        :return:
        """
        before_amount = int(self._accumulated_stock_item[f"{investor}_누"])
        today_amount = int(today_investor_trend)

        self._accumulated_stock_item[f"{investor}_누"] = str(before_amount + today_amount)

    def set_min_accumulated_purchase(self, investor):
        """
        투자자별로 최저 순매수량을 업데이트 한다.
        :param investor:
        :param accumulated_stock_item:
        :param last_stock_item:
        :return:
        """
        latest_accumulated_amount, before_min = int(self._accumulated_stock_item[f"{investor}_누"]), 0

        if len(self._updated_stock_item) != 0:
            before_min = int(self._updated_stock_item.iloc[-1, StockItem.COLUMNS.index(f"{investor}_최저")])

        if latest_accumulated_amount < 0:
            self._accumulated_stock_item[f"{investor}_최저"] = str(min(before_min, latest_accumulated_amount))
        else:
            self._accumulated_stock_item[f"{investor}_최저"] = str(before_min)

    def set_current_hodling_amount(self, investor):
        """
        투자자별로 현재 매집수량을 구한다
        :param investor:
        :param accumulated_stock_item:
        :return:
        """
        holding_amount: int = int(self._accumulated_stock_item[f"{investor}_누"]) - \
                              int(self._accumulated_stock_item[f"{investor}_최저"])

        self._accumulated_stock_item[f"{investor}_매집량"] = str(holding_amount)

    def set_max_holding_amount(self, investor):
        """
        투자자별로 최대 매집수량을 구한다.
        :param investor:
        :param accumulated_stock_item:
        :param last_stock_item:
        :return:
        """
        latest_holding_max, before_holding_max = int(self._accumulated_stock_item[f"{investor}_매집량"]), 0
        if len(self._updated_stock_item) != 0:
            before_holding_max = int(self._updated_stock_item.iloc[-1, StockItem.COLUMNS.index(f"{investor}_최고")])

        self._accumulated_stock_item[f"{investor}_최고"] = str(max(latest_holding_max, before_holding_max))

    def set_holding_ratio(self, investor):
        """
        투자자별로 매집 비율을 구한다.
        :param investor:
        :param accumulated_stock_item:
        :return:
        """
        holding_amount = int(self._accumulated_stock_item[f"{investor}_매집량"])
        holding_max = int(self._accumulated_stock_item[f"{investor}_최고"])

        if holding_max == 0:
            self._accumulated_stock_item[f"{investor}_분산율"] = "0"
        else:
            self._accumulated_stock_item[f"{investor}_분산율"] = f"{(holding_amount * 100) // holding_max}"

    def set_average_unit_price(self, investor):
        """
        각 투자자별 평균 단가를 계산합니다.
        :param investor:
        :return:
        """
        current_amount, current_average_unit = int(self._accumulated_stock_item[f"{investor}_매집량"]), 0
        prev_amount, prev_average_unit = 0, 0

        if len(self._updated_stock_item) != 0:
            prev_amount = int(self._updated_stock_item.iloc[-1, StockItem.COLUMNS.index(f"{investor}_매집량")])
            prev_average_unit = int(self._updated_stock_item.iloc[-1, StockItem.COLUMNS.index(f"{investor}_평단")])

        diff_amount = current_amount - prev_amount

        if diff_amount > 0:
            prev_money_amount = prev_amount * prev_average_unit
            diff_money_amount = diff_amount * int(self._accumulated_stock_item["현재가"])
            current_average_unit = (prev_money_amount + diff_money_amount) // current_amount
            self._accumulated_stock_item[f"{investor}_평단"] = str(current_average_unit)
        else:
            self._accumulated_stock_item[f"{investor}_평단"] = str(prev_average_unit)

    def set_margin_distribution_supply(self):
        """
        주식의 시세차익 유통물량을 구한다
        :param accumulated_stock_item:
        :return:
        """
        total_margin_distribution_supply = 0

        for investor in StockItem.TARGET_INVESTORS:
            total_margin_distribution_supply += int(self._accumulated_stock_item[f"{investor}_최고"])

        self._accumulated_stock_item["추_유통물량"] = str(total_margin_distribution_supply)

    def set_investor_stock_leading_per_investor(self):
        """
        투자자별로 주가 선도 비중을 구한다.
        :param accumulated_stock_item:
        :return:
        """
        total_margin_distribution_supply = int(self._accumulated_stock_item["추_유통물량"])

        for investor in StockItem.TARGET_INVESTORS:
            holding_max = int(self._accumulated_stock_item[f"{investor}_최고"])

            if total_margin_distribution_supply != 0:
                self._accumulated_stock_item[f"{investor}_주가선도"] = \
                    f"{(holding_max * 100) // total_margin_distribution_supply}"
            else:
                self._accumulated_stock_item[f"{investor}_주가선도"] = "0"

    def make_influence_holding_amount(self):
        """
        "외국인투자자", "금융투자", "보험", "투신", "은행", "연기금등"의 합산 매집량을 계산한다.
        :param accumulated_stock_item:
        :return:
        """
        influence_sum = 0

        for investor in StockItem.TARGET_INVESTORS:
            if investor is not StockItem.PERSON:
                influence_sum += int(self._accumulated_stock_item[f"{investor}_매집량"])

        self._accumulated_stock_item["세력합"] = str(influence_sum)