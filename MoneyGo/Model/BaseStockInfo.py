import pandas as pd


class BaseStockInfo:
    MODEL = {'code': []}
    KEYS = ["code", "company_name", "listing_date", "update_date", "business"]

    def __init__(self, base_stock_info, table_name):
        self.market_db_table_name: str = table_name
        if isinstance(base_stock_info, pd.Series):
            self._base_stock_info = base_stock_info.to_dict()
        else:
            self._base_stock_info = base_stock_info

    @classmethod
    def get_empty_base_stock_info(cls) -> dict:
        return {k: '0' for k in cls.KEYS}

    def get_stock_code(self) -> str:
        return self._base_stock_info['code']

    def get_company_name(self) -> str:
        return self._base_stock_info['company_name']

    def get_listing_date(self) -> str:
        return self._base_stock_info["listing_date"]

    def get_update_date(self) -> str:
        return self._base_stock_info["update_date"]

    def set_update_date(self, update_date):
        self._base_stock_info["update_date"] = update_date
