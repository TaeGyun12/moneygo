class ApiQuery:
	TR_TIME_INTERVAL = 0.2
	TR_TIME_SECOND = 1
	TR_MAX_QUERY = 101


class MysqlConfig:
	USER = 'root'
	PASSWORD = 'qwer!6819400'
	HOST = 'localhost'
	PORT = 3306
	CHARSET = 'utf8'
	DB_STOCK_BASE_INFO = 'db_stock_base_info'
	DB_STOCK_TREND_INFO = 'db_stock_trend_info'
	TABLE_KOSPI_BASE_INFO = 'kospi_base_info'
	TABLE_KOSDAQ_BASE_INFO = 'kosdaq_base_info'
	TABLE_LAST_STOCK_MARKET_DAY = 'last_stock_market_day'
	DB_LIST = [DB_STOCK_BASE_INFO, DB_STOCK_TREND_INFO]


class ExitCode:
	RETRY = "100"
	END = "200"
	HANG = "300"

class StockConfig:
	FIRST_CLASSIFICATION_DATE = "20120716"
