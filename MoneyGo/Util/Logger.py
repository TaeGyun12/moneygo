import logging
import os
from datetime import *


class Logger:
    default_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'Log')

    @classmethod
    def get_logger(cls, name: str):
        cls.logger = logging.getLogger(name)
        cls.logger.setLevel(logging.DEBUG)

        #  handler 추가
        file_name = os.path.join(cls.default_path,  date.today().isoformat().replace('-', '') + '_' + name)
        file_handler = logging.FileHandler(file_name, mode='a', encoding='utf8')

        # formatter 추가
        formatter = logging.Formatter('[%(levelname)s] - [%(asctime)s] %(name)s : %(message)s')
        file_handler.setFormatter(formatter)

        cls.logger.addHandler(file_handler)

        return cls.logger
