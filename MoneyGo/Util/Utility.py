from datetime import *

class Utility:
    @classmethod
    def change_value_format(cls, data: str) -> str:
        return data if data[0] != '+' and data[0] != '-' else data[1:]

    @classmethod
    def get_datetime(cls, date: str, time_span: int) -> datetime:
        return datetime(int(date[0:4]), int(date[4:6]), int(date[6:])) + timedelta(days=time_span)

