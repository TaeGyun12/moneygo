import subprocess
from Collector import *


class Scheduler:
    def __init__(self):
        self._collector_path = os.path.abspath('Collector.py')
        self.logger = Logger.get_logger('Scheduler')
        self._is_over = False

    def run_money_go(self, file_name: str, arg: str = "0") -> subprocess.CompletedProcess:
        return subprocess.run(args=[sys.executable, file_name, arg], capture_output=True, encoding='utf8', text=true)

    def is_impossible_time(self):
        """
        자정 넘어서는 키움 증권
        :return:
        """
        time = datetime.now()
        hour, mintue = time.hour, time.minute

        if hour >= 23 and mintue >= 58:
            return False
        elif hour == 0 and mintue < 2:
            return False
        else:
            return True

    def start(self, arg: str = "0"):
        self.logger.info('Scheduler Start')
        while not self._is_over:
            if self.is_impossible_time():
                result: subprocess.CompletedProcess = self.run_money_go(self._collector_path, arg)

                # 아직 업데이트 할 내용이 남았다.
                if result.stderr.strip() == ExitCode.END:
                    self._is_over = True

            timer.sleep(0.5)
        self.logger.info('Scheduler End')


if __name__ == "__main__":
    run_mode = len(sys.argv)
    scheduler: Scheduler = Scheduler()
    if len(sys.path) > 1:
        run_mode = sys.argv[1]
        if run_mode == "kospi":
            scheduler.start(run_mode)
        elif run_mode == "kosdaq":
            scheduler.start(run_mode)
        else:
            print("Not Supported Option")
