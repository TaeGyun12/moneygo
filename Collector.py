from collections import *
from MoneyGo.Connector.KiwoomConnector import *
from MoneyGo.Connector.MySqlConnector import *
from MoneyGo.Util.Logger import *
from MoneyGo.Model.BaseStockInfo import *


class Collector:
    def __init__(self):
        self.app = QApplication(sys.argv)
        self._kiwoom_connector: KiwoomConnector = KiwoomConnector()
        self._mysql_connector: MysqlConnector = MysqlConnector()
        self.logger = Logger.get_logger('Collector')

    def update_stock_investor_trend(self, stock_item: StockItem, latest_update: datetime, today: datetime):
        """
        주식의 지표를 업데이트 한다.
        :param stock_item:
        :param latest_update:
        :param today:
        :return:
        """
        for time_span in range(0, (today - latest_update).days + 1):
            target_day: datetime = latest_update + timedelta(days=time_span)
            success = self._kiwoom_connector.get_investor_trend_per_day(f"{target_day:%Y%m%d}", stock_item.code)
            target_day_investor_trend: dict = self._kiwoom_connector.get_inverstor_trend_data()

            if not success:
                self.logger.info(f'[{time_span} / {(today - latest_update).days}]'
                                 f'{stock_item.company_name} 키움 증권 통신 Fail')
                return False, stock_item.get_updated_stock_item()

            if self.is_validate_date(target_day_investor_trend, target_day):
                stock_item.update_indicator_per_investor(target_day_investor_trend)

                stock_item.set_margin_distribution_supply()

                stock_item.set_investor_stock_leading_per_investor()

                stock_item.make_influence_holding_amount()

                stock_item.add_current_accumulated_stock_item(f"{target_day:%Y%m%d}")

        self.logger.info(f'{stock_item.company_name} Update Completed')
        return True, stock_item.get_updated_stock_item()

    def is_validate_date(self, trend_data, target_day):
        """
        키움 증권에서 받아온 trend data가 유요한 날짜의 데이터인지 확인한다.
        :param trend_data:
        :param target_day:
        :return:
        """
        return trend_data and trend_data["일자"] == f"{target_day:%Y%m%d}"

    def get_latest_update_date(self, base: BaseStockInfo, stock: pd.DataFrame) -> datetime:
        """
        주식의 마지막 업데이트 날짜를 datetime 형태로 리턴해준다
        :param base:
        :param stock:
        :return:
        """
        base_update_date = base.get_update_date()
        base_listing_date = base.get_listing_date()

        if stock is None and base_update_date == '0':
            start_date = base_listing_date if base_listing_date > StockConfig.FIRST_CLASSIFICATION_DATE \
                else StockConfig.FIRST_CLASSIFICATION_DATE

            return Utility.get_datetime(start_date, 0)
        elif stock is None and base_update_date != '0':
            self.logger.critical(f"base: {base_update_date} 인데 Stock은 None 입니다.")
            raise Exception('Stock is None But base_update_date not 0')
        elif stock is not None and base_update_date != stock.iloc[0, 0]:
            self.logger.critical(f"base: {base_update_date}, stock : {stock.iloc[0, 0]} 입니다.")
            raise Exception('Update Date Not Matched')
        else:
            return Utility.get_datetime(base.get_update_date(), 1)

    def _get_corrected_stock_criterias(self, update_data: pd.DataFrame, listing_date: str) -> pd.DataFrame:
        """
        첫 번째 업데이트가 아닐 경우 마지막 업데이트된 Row가 DataFrame에 남아있어서 보정합니다.
        :param update_data:
        :param listing_date:
        :return:
        """
        first_row_update_date = update_data.iloc[0, 0]
        first_update_date = listing_date if listing_date > StockConfig.FIRST_CLASSIFICATION_DATE \
            else StockConfig.FIRST_CLASSIFICATION_DATE

        if first_row_update_date != first_update_date:
            return update_data.drop(0)
        else:
            return update_data

    def _check_all_db_and_make_not_exist_db(self):
        """
        필수적으로 존재해야 하는 db가 있는지 확인하고 없으면 만든다.
        :return:
        """
        current_db_list = self._mysql_connector.show_database()

        for db_name in MysqlConfig.DB_LIST:
            if db_name not in current_db_list:
                self.logger.info(f"{db_name} not exist")
                self._mysql_connector.create_database(db_name)

        self.logger.info("All DB Exist")

    def _check_and_make_kospi_base_info(self):
        """
        kospi_base_info table이 없으면 생성합니다.
        :return:
        """
        if not self._mysql_connector.check_table(MysqlConfig.DB_STOCK_BASE_INFO, MysqlConfig.TABLE_KOSPI_BASE_INFO):
            self.logger.info("Kospi Base Table Create")
            kospi_info: pd.DataFrame = self._kiwoom_connector.get_all_kospi_informations()
            self._mysql_connector.register_whole_stock_base_info(MysqlConfig.TABLE_KOSPI_BASE_INFO, kospi_info)

    def _check_and_make_kosdaq_base_info(self):
        """
        kosdaq_base_info table이 없으면 생성합니다.
        :return:
        """
        if not self._mysql_connector.check_table(MysqlConfig.DB_STOCK_BASE_INFO, MysqlConfig.TABLE_KOSDAQ_BASE_INFO):
            self.logger.info("Kosdaq Base Table Create")
            kosdaq_info: pd.DataFrame = self._kiwoom_connector.get_all_kosdaq_informations()
            self._mysql_connector.register_whole_stock_base_info(MysqlConfig.TABLE_KOSDAQ_BASE_INFO, kosdaq_info)

    def _check_and_make_stock_market_info(self):
        """
        last_stock_market_day table이 없으면 생성합니다.
        :return:
        """
        if not self._mysql_connector.check_table(MysqlConfig.DB_STOCK_BASE_INFO,
                                                 MysqlConfig.TABLE_LAST_STOCK_MARKET_DAY):
            self.logger.info("stock market Table Create")
            self._mysql_connector.create_last_stock_market_day()

    def _make_base_db_configurations(self):
        """
        데이터를 쌓기위한 DB의 기본환경 셋팅을 확인하고 없는 DB or Table은 생성합니다.
        :return:
        """
        self.logger.info('DB Check Start')
        self._check_all_db_and_make_not_exist_db()
        self._check_and_make_kospi_base_info()
        self._check_and_make_kosdaq_base_info()
        self._check_and_make_stock_market_info()
        self.logger.info('DB Check End Successfully')

    def update_all_stock_criteria(self):
        """
        kospi, kosdaq 모든 종목을 업데이트 합니다.
        :return:
        """
        try:
            self._setup_and_update_all_stock()
        except Exception as e:
            self.logger.error(f'Exception Occurred, {e}', exc_info=true)
            exit(ExitCode.END)

    def _setup_and_update_all_stock(self):
        """
        종목 업데이트를 위한 사전 준비 및 종목에 대한 정보를 업데이트
        :return:
        """
        self._make_base_db_configurations()
        # kospi, kosdq 코드 추가

        exit(ExitCode.END)

    def update_all_kospi_stock_criteria(self):
        """
        kospi 모든 종목을 업데이트 합니다.
        :return:
        """
        try:
            self._setup_and_update_all_kospi_stock()
        except Exception as e:
            self.logger.critical(f'Exception Occurred, {e}', exc_info=true)
            exit(ExitCode.END)

    def update_all_kosdaq_stock_criteria(self):
        """
        kosdaq 모든 종목을 업데이트 합니다.
        :return:
        """
        try:
            self._setup_and_update_all_kosdaq_stock()
        except Exception as e:
            self.logger.critical(f'Exception Occurred, {e}', exc_info=true)
            exit(ExitCode.END)

    def _setup_and_update_all_kospi_stock(self):
        """
        kospi 종목의 업데이트를 위한 셋업동작과 업데이트를 시작합니다.
        :return:
        """
        self._make_base_db_configurations()
        self._update_today_kospi_stocks()

        exit(ExitCode.END)

    def _setup_and_update_all_kosdaq_stock(self):
        """
        kosdaq 종목의 업데이트를 위한 셋업동작과 업데이트를 시작합니다.
        :return:
        """
        self._make_base_db_configurations()
        self._update_today_kosdaq_stocks()

        exit(ExitCode.END)

    def _get_stock_market_date(self) -> datetime:
        """
        장날을 가져옵니다.
        :return:
        """
        stock_date: str = self._mysql_connector.get_stock_market_date()
        if stock_date == '0':
            return datetime.today()
        else:
            return Utility.get_datetime(stock_date, 0)

    def _is_update_end(self, stock_list):
        """
        업데이트할 종목들이 있는지 확인합니다.
        :param stock_list:
        :return:
        """
        if len(stock_list) == 0:
            return True
        else:
            return False

    def _update_today_kospi_stocks(self):
        """
        특정 kospi 종목을 선택해서 수급 지표를 업데이트합니다.
        :return:
        """
        self.logger.info('Kospi Update Start')
        end_date: datetime = self._get_stock_market_date()
        kospi_stock_list = self._mysql_connector.get_not_updated_stock_list(MysqlConfig.TABLE_KOSPI_BASE_INFO,
                                                                            f"{end_date:%Y%m%d}")
        if self._is_update_end(kospi_stock_list) is True:
            exit(ExitCode.END)

        update_target_stock: BaseStockInfo = BaseStockInfo(kospi_stock_list.iloc[0], MysqlConfig.TABLE_KOSPI_BASE_INFO)
        self._update_investor_criteria_to_end_date(update_target_stock, end_date)

    def _update_today_kosdaq_stocks(self):
        """
        특정 kosdaq 종목을 선택해서 수급 지표를 업데이트합니다.
        :return:
        """
        self.logger.info('Kosdaq Update Start')
        end_date: datetime = self._get_stock_market_date()
        kosdaq_stock_list = self._mysql_connector.get_not_updated_stock_list(MysqlConfig.TABLE_KOSDAQ_BASE_INFO,
                                                                             f"{end_date:%Y%m%d}")
        if self._is_update_end(kosdaq_stock_list) is True:
            exit(ExitCode.END)

        update_target_stock: BaseStockInfo = BaseStockInfo(kosdaq_stock_list.iloc[0], MysqlConfig.TABLE_KOSDAQ_BASE_INFO)
        self._update_investor_criteria_to_end_date(update_target_stock, end_date)

    def _update_today_stock_day(self, date: str):
        """
        업데이트한 날에서 가장 가까운 장날을 업데이트 합니다.
        :param date:
        :return:
        """
        self._mysql_connector.register_stock_market_date(date)

    def _check_extreme_value_change_ouccured(self, code, last_stock_item: pd.DataFrame, latest_update: datetime) \
            -> bool:
        """
        증자, 감자, 액면분할등에 따른 급격한 주식 가격 변동을 확인합니다.
        :param code:
        :param last_stock_item:
        :param latest_update:
        :return:
        """
        if last_stock_item is None:
            return False
        else:
            success = self._kiwoom_connector.get_investor_trend_per_day(f"{latest_update:%Y%m%d}", code)
            target_day_investor_trend: dict = self._kiwoom_connector.get_inverstor_trend_data()

            if success is True and target_day_investor_trend is not None:
                prev_value = int(last_stock_item.iloc[0, 1])
                today_value = int(target_day_investor_trend["현재가"])
                if today_value > prev_value and today_value >= (prev_value * 1.5):
                    self.logger(f"{code}, 감자, 액면분할이 의심됩니다. (prev: {prev_value}, today: {today_value})")
                    return True
                elif prev_value > today_value and prev_value >= (today_value * 1.5):
                    self.logger(f"{code}, 증자가 의심됩니다. (prev: {prev_value}, today: {today_value})")
                    return True
                else:
                    return False
            else:
                raise Exception('이전 가격과 금일 가격 비교 시, 키움 Request Fail')

    def clear_stock_all_indicator(self, code):
        """
        종목의 모든 업데이트 정보를 삭제합니다.
        :param code:
        :return:
        """
        self._mysql_connector.register_stock_update_date(MysqlConfig.TABLE_KOSPI_BASE_INFO, code, 0)
        self._mysql_connector.drop_table(MysqlConfig.DB_STOCK_TREND_INFO, code)

    def update_latest_stock_indicator(self, stock: BaseStockInfo, indicator: pd.DataFrame, last_update_date: str, is_end: bool):
        """
        종목의 모든 투자 지표를 업데이트합니다.
        :param stock:
        :param indicator:
        :param last_update_date:
        :param is_end:
        :return:
        """
        self._mysql_connector.register_stock_trend(stock.get_stock_code(), indicator)
        self._mysql_connector.register_stock_update_date(stock.market_db_table_name, stock.get_stock_code(),
                                                         int(last_update_date))

        if is_end is True:
            self._update_today_stock_day(last_update_date)

    def _update_investor_criteria_to_end_date(self, base_stock_info: BaseStockInfo, end_date: datetime):
        """
        특정 종목에 대한 수급 지표를 업데이트합니다.
        :param base_stock_info:
        :param end_date:
        :return:
        """
        self.logger.info(f'Stock Update Start {base_stock_info.get_company_name()}')
        latest_stock_item: pd.DataFrame = self._mysql_connector.get_stock_latest_trend(base_stock_info.get_stock_code(),
                                                                                       base_stock_info.get_update_date())
        latest_update_date: datetime = self.get_latest_update_date(base_stock_info, latest_stock_item)

        if self._check_extreme_value_change_ouccured(base_stock_info.get_stock_code(),
                                                     latest_stock_item, latest_update_date) is True:
            self.logger.info(f"{base_stock_info.get_company_name()}, 주식 가격의 큰 변동이 의심됩니다.")
            self.clear_stock_all_indicator(base_stock_info.get_stock_code())
        else:
            stock_item: StockItem = StockItem(base_stock_info.get_stock_code(), base_stock_info.get_company_name())
            stock_item.initialize_update_info(latest_stock_item)
            (isEnd, update_stock_info) = self.update_stock_investor_trend(stock_item, latest_update_date, end_date)
            corrected_stock_info = self._get_corrected_stock_criterias(update_stock_info,
                                                                       base_stock_info.get_listing_date())

            last_update_date = corrected_stock_info.iloc[-1, 0]
            self.update_latest_stock_indicator(base_stock_info, corrected_stock_info, last_update_date, isEnd)

        exit(ExitCode.RETRY)

    def retry_or_end_update_criteria(self, isEnd):
        """
        업데이트를 계속할지 아니면 끝낼지 결정합니다.
        :param isEnd:
        :return:
        """
        if isEnd:
            exit(ExitCode.END)
        else:
            exit(ExitCode.RETRY)

    def update_investor_criteria(self, stock_code: str):
        """
        특정 종목에 대한 투자자별 모든 지표를 업데이트합니다.
        :param stock_code:
        :return:
        """
        try:
            self._update_investor_criteria_all(stock_code)
        except Exception as e:
            self.logger.error(f"{stock_code} : Error Occurred, {e}", exc_info=true)
            exit(ExitCode.END)

    def _update_investor_criteria_all(self, stock_code: str):
        """
        특정 종목 하나만을 업데이트하기 위한 테스트 함수
        :param stock_code:
        :return:
        """
        MysqlConfig.DB_STOCK_BASE_INFO = 'test'
        MysqlConfig.DB_STOCK_TREND_INFO = 'test'

        # 1. 한 종목의 주식 기본 정보를 가져온다.
        base_stock_info: BaseStockInfo = BaseStockInfo(
            self._mysql_connector.get_stock_base_info(MysqlConfig.TABLE_KOSDAQ_BASE_INFO, stock_code).iloc[0])

        # 2. 최신 업데이트된 Row를 읽어온다.
        latest_stock_item = self._mysql_connector.get_stock_latest_trend(stock_code, base_stock_info.get_update_date())
        latest_update = self.get_latest_update_date(base_stock_info, latest_stock_item)

        # 3. 해당 종목의 정보를 쌓는다.
        stock_item: StockItem = StockItem(base_stock_info.get_stock_code(), base_stock_info.get_company_name())
        stock_item.initialize_update_info(latest_stock_item)
        (isEnd, update_stock_info) = self.update_stock_investor_trend(stock_item, latest_update, datetime.today())
        corrected_stock_info = self._get_corrected_stock_criterias(update_stock_info, base_stock_info.get_listing_date())

        # 4. SQL에 Update Date 업데이트
        last_update_date = corrected_stock_info.iloc[-1, 0]
        self._mysql_connector.register_stock_trend(stock_code, corrected_stock_info)
        self._mysql_connector.register_stock_update_date(MysqlConfig.TABLE_KOSDAQ_BASE_INFO,
                                                         stock_code, last_update_date)

        self.retry_or_end_update_criteria(isEnd)


if __name__ == "__main__":
    stock_code: str = sys.argv[1]
    give_me_the_money = Collector()

    if stock_code == "kospi":
        give_me_the_money.update_all_kospi_stock_criteria()
    elif stock_code == "kosdaq":
        MysqlConfig.DB_STOCK_TREND_INFO = 'test'
        MysqlConfig.DB_STOCK_BASE_INFO = 'test'
        give_me_the_money.update_all_kosdaq_stock_criteria()
    elif stock_code.isalnum():
        give_me_the_money.update_investor_criteria(stock_code)
    else:
        give_me_the_money.update_all_stock_criteria()
