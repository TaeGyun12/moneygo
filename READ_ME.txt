# Mission
각 세력들의 수급 정보를 바탕으로 투자를 할 수 있는 시스템을 구축한다. 시스템의 단계는 아래와 같은 단계로 완성해나간다.

1. 데이터 저장  각 투자자 별 수급 현황에 대한 데이터를 각 종목별로 쌓고 이를 볼 수 있다.
2. 데이터 조회  DB에 쌓인 데이터를 웹을 통해 볼 수 있도록 한다.
3. 종목 찾기  웹에서 스윙 투자 조건, 수급 조건을 넣어서 종목을 선정한다.
4. 시뮬레이션  2번째 단계에서 선정한 조건들을 넣어서 백 테스팅을 통해 수익 확률이 높은 기법인지 확인할 수 있다.

# 진행 사항
1. 특정 종목에 대한 투자자별 기본 지표 정보를 DB에 저장하는 기능 완료

# 파일
1. KiwoomConnector  키움 증권에 API를 날리기 위한 모듈
2. MysqlConnector  MySQL에 대한 작업을하기 위한 모듈
3. BaseStockInfo  주식의 기본 정보에 대한 Model
4. StockItem  주식 별 수급 지표 데이터에 대한 Model
5. Collector  Kospi, Kosdaq 모든 종목에 대해서 지표 데이터를 업데이트하는 모듈
6. Scheduler  Collector를 실행 시켜주는 Module
